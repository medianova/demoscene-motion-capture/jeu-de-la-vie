var socket

const width = 750
const height = 500

var points = []

function setup() {

    // WebSocket protocol initialisation
    socket = io.connect('http://127.0.0.1:5000/');
    socket.on('connect', () => {
        socket.emit(
            'client-connect', { client: 'jeu-de-la-vie', isConnecting: true }
        )
    })
    socket.on('disconnect', () => {
        socket.emit(
            'client-disconnect', { client: 'jeu-de-la-vie', isConnecting: false }
        )
        fetch(`http://127.0.0.1:5000/api/sample/perlin_noise?width=${width}&height=${height}&run=false`)
    })
    socket.on('perlin-noise-points', (msg) => {
        points = msg
    })

    // Run Api command
    fetch(`http://127.0.0.1:5000/api/sample/perlin_noise?width=200&height=200&run=true&delay=0.05&nb=2`)
        .then(reponse => reponse.json())
        .then(data => console.log(data))

    createCanvas(width, height)
    background(225)

}

function draw() {
    background(225)
    for (const point of points) {
        console.log(point.x, point.y)
        ellipse(point.x, point.y, 3, 3)
    }
}